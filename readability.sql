-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 22, 2015 at 12:29 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `readability`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE IF NOT EXISTS `accounts` (
`account_id` int(11) NOT NULL,
  `email` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `role` varchar(45) NOT NULL,
  `admin_role` tinyint(4) DEFAULT '0',
  `is_deleted` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`account_id`, `email`, `password`, `role`, `admin_role`, `is_deleted`) VALUES
(1, 'admin', 'admin', 'admin', 1, 0),
(2, 'teacher', 'teacher', 'teacher', 0, 0),
(3, 'student22', 'student', 'student', 0, 0),
(4, 'asd', 'asd', 'student', 0, 1),
(5, 'asASAS', '123213', 'student', 0, 1),
(6, 'asd@asd.com', 'asd@asd.com', 'student', 0, 1),
(8, 'test2', 'test', 'teacher', 0, 1),
(9, 'soys', 'soy', 'teacher', 0, 0),
(10, 'aaaaa', '', 'student', 0, 1),
(11, '', '', 'student', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE IF NOT EXISTS `banner` (
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`message`) VALUES
('test');

-- --------------------------------------------------------

--
-- Table structure for table `student_attendance`
--

CREATE TABLE IF NOT EXISTS `student_attendance` (
`attendance_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `id_number` int(11) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `time_in` varchar(45) DEFAULT NULL,
  `time_out` varchar(45) DEFAULT NULL,
  `status` varchar(45) DEFAULT 'N/A',
  `teacher` varchar(45) DEFAULT 'N/A'
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_attendance`
--

INSERT INTO `student_attendance` (`attendance_id`, `account_id`, `id_number`, `date`, `time_in`, `time_out`, `status`, `teacher`) VALUES
(13, 3, 3000, '07-Jun-2014', '5:44:32 PM', '9:53:47 AM', 'Finished', 'Aila Joy Santos'),
(17, 6, 123213, '14-Jun-2014', '12:51:19 PM', '12:53:16 PM', 'Cancelled', 'Aila Joy Santos'),
(18, 3, 3000, '19-Oct-2014', '9:50:12 AM', '10:05:13 AM', 'Cancelled', 'test2'),
(19, 3, 3000, '01-Aug-2015', '11:47:19 AM', '11:47:25 AM', 'Finished', 'test2'),
(20, 0, 3000, '02-Aug-2015', '11:30:49 AM', NULL, 'N/A', 'test2'),
(22, 3, 3000, '02-Aug-2015', '12:10:59 PM', '12:11:01 PM', 'Finished', 'test2');

-- --------------------------------------------------------

--
-- Table structure for table `student_information`
--

CREATE TABLE IF NOT EXISTS `student_information` (
`student_id` int(11) NOT NULL,
  `account_id` int(11) DEFAULT NULL,
  `id_number` int(11) DEFAULT NULL,
  `full_name` varchar(100) DEFAULT NULL,
  `nick_name` varchar(100) DEFAULT NULL,
  `date_of_birth` date DEFAULT NULL,
  `age_years` tinyint(4) DEFAULT NULL,
  `age_months` tinyint(4) DEFAULT NULL,
  `home_address` varchar(100) DEFAULT NULL,
  `phone` varchar(100) NOT NULL,
  `school` varchar(100) DEFAULT NULL,
  `level` varchar(100) DEFAULT NULL,
  `school_address` varchar(100) NOT NULL,
  `contact_person_in_school_and_designation` varchar(100) DEFAULT NULL,
  `father_name` varchar(100) NOT NULL,
  `father_mobile` int(11) NOT NULL,
  `father_office` int(11) NOT NULL,
  `father_email` varchar(100) NOT NULL,
  `father_occupation` varchar(100) NOT NULL,
  `mother_name` varchar(100) NOT NULL,
  `mother_mobile` int(11) NOT NULL,
  `mother_office` int(11) NOT NULL,
  `mother_email` varchar(100) NOT NULL,
  `mother_occupation` varchar(100) NOT NULL,
  `number_of_siblings` tinyint(4) DEFAULT NULL,
  `childs_position_in_the_family` tinyint(4) DEFAULT NULL,
  `languages_spoken_at_home` varchar(100) NOT NULL,
  `observed_difficulties_at_home` text,
  `reported_difficulties_at_school` text,
  `are_there_members_of_the_family_who_have_learning_difficulties` tinyint(4) DEFAULT NULL,
  `if_yes_please_specify_the_difficulty_and_the_relationship_to_the` text,
  `does_the_student_have_remedial_teaching_at_school_or_elsewhere` tinyint(4) DEFAULT NULL,
  `if_yes_state_where_and_how_often` text,
  `has_the_student_been_evaluated_by_a_developmental_pediatrician_o` tinyint(4) DEFAULT NULL,
  `when_question` varchar(100) DEFAULT NULL,
  `name_of_evaluator` varchar(100) DEFAULT NULL,
  `is_the_report_available` tinyint(4) DEFAULT NULL,
  `vision_tested` tinyint(4) DEFAULT NULL,
  `date_and_results` text,
  `hearing_tested` tinyint(4) DEFAULT NULL,
  `hearing_tested_date_and_results` text,
  `what_is_the_students_general_attitude_towards_school_and_learnin` text,
  `your_childs_interests` text,
  `concerns_you_want_us_to_know` text
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_information`
--

INSERT INTO `student_information` (`student_id`, `account_id`, `id_number`, `full_name`, `nick_name`, `date_of_birth`, `age_years`, `age_months`, `home_address`, `phone`, `school`, `level`, `school_address`, `contact_person_in_school_and_designation`, `father_name`, `father_mobile`, `father_office`, `father_email`, `father_occupation`, `mother_name`, `mother_mobile`, `mother_office`, `mother_email`, `mother_occupation`, `number_of_siblings`, `childs_position_in_the_family`, `languages_spoken_at_home`, `observed_difficulties_at_home`, `reported_difficulties_at_school`, `are_there_members_of_the_family_who_have_learning_difficulties`, `if_yes_please_specify_the_difficulty_and_the_relationship_to_the`, `does_the_student_have_remedial_teaching_at_school_or_elsewhere`, `if_yes_state_where_and_how_often`, `has_the_student_been_evaluated_by_a_developmental_pediatrician_o`, `when_question`, `name_of_evaluator`, `is_the_report_available`, `vision_tested`, `date_and_results`, `hearing_tested`, `hearing_tested_date_and_results`, `what_is_the_students_general_attitude_towards_school_and_learnin`, `your_childs_interests`, `concerns_you_want_us_to_know`) VALUES
(1, 1, 1000, 'Kelvin John Santos', NULL, NULL, NULL, NULL, NULL, '0', NULL, NULL, '', NULL, '', 0, 0, '', '', '', 0, 0, '', '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '', '', '', NULL),
(2, 2, 2000, 'Aila Joy Santos', 'qeg', '0000-00-00', 127, 127, '13y', '1313', NULL, NULL, '', NULL, '', 0, 0, '', '', '', 0, 0, '', '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '', '', '', NULL),
(3, 3, 3000, 'Karl Joseph Santos', 'karl', '1992-10-30', 11, 2, '#6 J. Calma Street San Roque Dau 1st Lubao Pampanga', '09067489530', 'DHVTSU', '1', 'San Fernando2', 'Soy', 'Romi', 1234, 4321, 'asd@asd.com', 'Admin', 'Marichu', 9807987, 6781356, 'zxc@zxc.com', 'OFW', 3, 2, 'English', 'AAAAAAAAAAAAAAAAAAAA', 'BBBBBBBBBBBBBBBBBBBB', 1, 'CCCCCCCCCCCCCCCCCC', 1, 'DDDDDDDDDDDDDDDDDD', 1, 'Now', 'GGGGGGGGGGGGG', 1, 1, 'QWQWQWQ', 1, 'ASASASASA', 'GDGDGDGD', 'CXBCBCBC', 'HRHRHRHR'),
(4, 4, 0, 'asd', 'asd', '0000-00-00', 0, 0, '', '0', '', '', '', '', '', 0, 0, '', '', '', 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, '', '', 0, 0, '', 0, '', '', '', ''),
(5, 5, 0, 'ASASAS', '', '0000-00-00', 0, 0, '', '0', '', '', '', '', '', 0, 0, '', '', '', 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, '', '', 0, 0, '', 0, '', '', '', ''),
(6, 6, 1000, 'qefqefqe', 'qefqef', '0000-00-00', 0, 0, 'qef', '0', 'fqe', 'fqe', 'qef', 'qef', 'qef', 0, 0, 'qef', 'qfe', 'qef', 0, 0, 'qef', 'qef', 0, 0, 'qef', 'qef\r\n\r\n', 'qef', 0, 'qef', 0, 'qef', 0, 'qef', 'qef', 0, 0, 'qef\r\n', 0, 'qef', 'qef\r\n', 'qef', 'qef'),
(7, 10, 0, '', '', '0000-00-00', 0, 0, '', '0', '', '', '', '', '', 0, 0, '', '', '', 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, '', '', 0, 0, '', 0, '', '', '', ''),
(8, 11, 0, '', '', '0000-00-00', 0, 0, '', '0', '', '', '', '', '', 0, 0, '', '', '', 0, 0, '', '', 0, 0, '', '', '', 0, '', 0, '', 0, '', '', 0, 0, '', 0, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_attendance`
--

CREATE TABLE IF NOT EXISTS `teacher_attendance` (
`attendance_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `id_number` int(11) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `time_in` varchar(45) DEFAULT NULL,
  `time_out` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher_attendance`
--

INSERT INTO `teacher_attendance` (`attendance_id`, `account_id`, `id_number`, `date`, `time_in`, `time_out`) VALUES
(19, 8, 123123, '19-Oct-2014', '10:16:41 AM', '10:17:34 AM'),
(20, 9, 1000, '19-Oct-2014', '10:19:49 AM', '10:19:57 AM'),
(23, 9, 1000, '02-Aug-2015', '12:08:56 PM', '12:08:59 PM');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_information`
--

CREATE TABLE IF NOT EXISTS `teacher_information` (
`teacher_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `id_number` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `nick_name` varchar(100) NOT NULL,
  `date_of_birth` date NOT NULL,
  `age_years` tinyint(4) NOT NULL,
  `age_months` tinyint(4) NOT NULL,
  `home_address` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teacher_information`
--

INSERT INTO `teacher_information` (`teacher_id`, `account_id`, `id_number`, `full_name`, `nick_name`, `date_of_birth`, `age_years`, `age_months`, `home_address`, `phone`) VALUES
(1, 8, 123123, 'test2', 'test', '2014-10-25', 11, 5, 'test2', '0'),
(2, 9, 1000, 'Soy', 'Soy', '2014-10-22', 11, 5, 'Address', '0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
 ADD PRIMARY KEY (`account_id`);

--
-- Indexes for table `student_attendance`
--
ALTER TABLE `student_attendance`
 ADD PRIMARY KEY (`attendance_id`);

--
-- Indexes for table `student_information`
--
ALTER TABLE `student_information`
 ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `teacher_attendance`
--
ALTER TABLE `teacher_attendance`
 ADD PRIMARY KEY (`attendance_id`);

--
-- Indexes for table `teacher_information`
--
ALTER TABLE `teacher_information`
 ADD PRIMARY KEY (`teacher_id`), ADD KEY `teacher_id` (`teacher_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `student_attendance`
--
ALTER TABLE `student_attendance`
MODIFY `attendance_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `student_information`
--
ALTER TABLE `student_information`
MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `teacher_attendance`
--
ALTER TABLE `teacher_attendance`
MODIFY `attendance_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `teacher_information`
--
ALTER TABLE `teacher_information`
MODIFY `teacher_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
