<?php 

if (isset($jquerydatatablescss)) {
	$jquerydatatablescss = $jquerydatatablescss;
} else {
	$jquerydatatablescss = "";
}

if ($jquerydatatablescss == "1") {
	echo '<link rel="stylesheet" href="../../public_html/css/jquery.dataTables.min.css" />';
}

if (isset($bootstrapcss)) {
	$bootstrapcss = $bootstrapcss;
} else {
	$bootstrapcss = "";
}

if ($bootstrapcss == "1") {
	echo '<link rel="stylesheet" href="../../public_html/css/bootstrap.min.css">';
}

if (isset($stickyfooternavbarcss)) {
	$stickyfooternavbarcss = $stickyfooternavbarcss;
} else {
	$stickyfooternavbarcss = "";
}

if ($stickyfooternavbarcss == "1") {
	echo '<link rel="stylesheet" href="../../public_html/css/sticky-footer-navbar.css">';
}

if (isset($signincss)) {
	$signincss = $signincss;
} else {
	$signincss = "";
}

if ($signincss == "1") {
	echo '<link rel="stylesheet" href="../../public_html/css/signin.css">';
}

if (isset($bootstrapdatepickercss)) {
	$bootstrapdatepickercss = $bootstrapdatepickercss;
} else {
	$bootstrapdatepickercss = "";
}

if ($bootstrapdatepickercss == "1") {
	echo '<link rel="stylesheet" href="../../public_html/css/datepicker.css">';
}

?>