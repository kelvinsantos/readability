<?php

if (isset($jqueryuijs)) {
	$jqueryuijs = $jqueryuijs;
} else {
	$jqueryuijs = "";
}

if ($jqueryuijs == "1") {
echo '<script src="../../public_html/js/jquery-ui-1.11.4/jquery-ui.min.js"></script>';
}

if (isset($bootstrapjs)) {
	$bootstrapjs = $bootstrapjs;
} else {
	$bootstrapjs = "";
}

if ($bootstrapjs == "1") {
echo '<script src="../../public_html/js/bootstrap.min.js"></script>';
}

if (isset($jqueryvalidatejs)) {
	$jqueryvalidatejs = $jqueryvalidatejs;
} else {
	$jqueryvalidatejs = "";
}

if ($jqueryvalidatejs == "1") {
echo '<script src="../../public_html/js/jquery.validate.min.js"></script>';
}

if (isset($jquerydatatablesjs)) {
	$jquerydatatablesjs = $jquerydatatablesjs;
} else {
	$jquerydatatablesjs = "";
}

if ($jquerydatatablesjs == "1") {
echo '<script src="../../public_html/js/jquery.dataTables.min.js"></script>';
}

if (isset($bootstrapdatepicker)) {
	$bootstrapdatepicker = $bootstrapdatepicker;
} else {
	$bootstrapdatepicker = "";
}

if ($bootstrapdatepicker == "1") {
echo '<script src="../../public_html/js/bootstrap-datepicker.js"></script>';
}

if (isset($typedjs)) {
	$typedjs = $typedjs;
} else {
	$typedjs = "";
}

if ($typedjs == "1") {
echo '<script src="../../public_html/js/typed.min.js"></script>';
}

if (isset($bootbox)) {
	$bootbox = $bootbox;
} else {
	$bootbox = "";
}

if ($bootbox == "1") {
echo '<script src="../../public_html/js/bootbox.js"></script>';
}

?>