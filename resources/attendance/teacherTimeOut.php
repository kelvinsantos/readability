<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
    // CSS
    $bootstrapcss = "1";
    $stickyfooternavbarcss = "1";
    
    // JS
    $jqueryjs = "1";
    $bootstrapjs = "1";
    $bootbox = "1";
    
    $title = "Teacher Time Out";
    $useUpdateClock = true;
    require_once(realpath(dirname(__FILE__) . "/../config.php"));
    require_once(TEMPLATES_PATH . "/header.php");

    $result = mysqli_query($mysqli,"SELECT * FROM teacher_attendance a INNER JOIN teacher_information b INNER JOIN accounts c ON a.account_id = b.account_id AND b.account_id = c.account_id WHERE a.account_id='".$_SESSION['accountId']."' AND a.date = DATE_FORMAT(CURDATE(), '%d-%b-%Y')");
    $row = mysqli_fetch_array($result)
?>

<script type="text/javascript">
$(function() {
    $("#save").click(function() {
        $.ajax({
          method: "POST",
          url: "teacherTimeOutService.php",
          data: jQuery("form").serialize(),
          success: function(response) {
                var response = $.parseJSON(response);
                bootbox.alert(response.message, function() {
                    window.location.href='teacherTimeIn.php';
                });
            }
        });
    });
});
</script>

<div class="container">
    <form method="POST" role="form" class="form-align">
        <input type="hidden" class="form-control" name="attendanceId" id="attendanceId" value="<?php echo $row['attendance_id'] ?>" readOnly>
        <h3><span class="label label-primary"><?php echo $title ?></span></h3>
        <br />
        <div class="form-group">
            <label>ID Number</label>
            <input type="text" class="form-control" name="idNumber" id="idNumber" value="<?php echo $row['id_number'] ?>" readOnly>
        </div>
        <div class="form-group">
            <label>Name</label>
            <input type="text" class="form-control" name="fullName" id="fullName" value="<?php echo $row['full_name'] ?>" readOnly>
        </div>
        <div class="form-group">
            <label>Date</label>
            <input type="text" class="form-control" name="date" id="date" value="<?php echo $row['date'] ?>" readOnly>
        </div>
        <div class="form-group">
            <label>Time In</label>
            <input type="text" class="form-control" name="updateTimeIn" id="updateTimeIn" value="<?php echo $row['time_in'] ?>" readOnly>
        </div>
        <div class="form-group">
            <label>Time Out</label>
            <input type="text" class="form-control" name="timeOut" id="timeOut" value="<?php echo $current_date ?>" readOnly>
        </div>
        <button type="button" class="btn btn-primary pull-right" name="save" id="save">Submit</button>
    </form>
</div>
<?php mysqli_close($mysqli); ?>

</tbody>
</table>
</div>

<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>