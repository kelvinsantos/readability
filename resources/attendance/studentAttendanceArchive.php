<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$jquerydatatablescss = "1";
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";
	$jquerydatatablesjs = "1";
?>

<?php 
	$title = "Student Attendance Archive";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");
?>

<script type="text/javascript">
	$(document).ready(function() {

	    // Setup - add a text input to each footer cell
	    $('#attendanceArchiveTable tfoot tr th').each(function() {
	        var title = $('#attendanceArchiveTable thead tr th').eq($(this).index()).text();
	        $(this).html('<input type="text" placeholder="Search '+title+'" />');
	    });
	 
	    // DataTable
	    var table = $('#attendanceArchiveTable').DataTable();
	 
	    // Apply the search
	    table.columns().eq(0).each(function(colIdx) {
	        $('input', table.column(colIdx).footer()).on('keyup change', function() {
	            table
	                .column(colIdx)
	                .search(this.value)
	                .draw();
	        });
	    });

		$('#attendanceArchiveTable #idx').on('click', function() {
			var accountId = $(this).attr('accountId');
			window.location.href = "/readability/resources/information/studentInformation.php?accountId=" + accountId;
		});
	
	});
</script>

<?php
$result = mysqli_query($mysqli,"SELECT * FROM student_attendance a INNER JOIN student_information b INNER JOIN accounts c ON a.account_id = b.account_id AND a.account_id = c.account_id WHERE role = 'student'");
?>
<div class='container'>
<div class="row">
  <div class="col-md-11">
  	<h3><span class="label label-primary">Student Attendance Archive</span></h3>
  </div>
  <div class="col-md-1" style="line-height: 56px;">
  	<button type="button" class="btn btn-primary" onclick="printPage()">
	  <span class="glyphicon glyphicon-print"></span> Print
	</button>
  </div>
</div>
<br />
	<table cellpadding='0' cellspacing='0' border='0' class='display' id='attendanceArchiveTable'>
        <thead>
            <tr>
                <th>ID Number</th>
                <th>Name</th>
                <th>Date</th>
                <th>Time In</th>
                <th>Time Out</th>
                <th>Status</th>
                <th>Teacher</th>
            </tr>
        </thead>

        <tfoot>
            <tr>
                <th>ID Number</th>
                <th>Name</th>
                <th>Date</th>
                <th>Time In</th>
                <th>Time Out</th>
                <th>Status</th>
                <th>Teacher</th>
            </tr>
        </tfoot>
    <tbody>

		<?php
		while($row = mysqli_fetch_array($result))
		  {
		  echo "<tr id='idx' accountId=".$row['account_id'].">";
		  echo "<td>".$row['id_number']."</td>";
		  echo "<td>".$row['full_name']."</td>";
		  echo "<td>".$row['date']."</td>";
		  echo "<td>".$row['time_in']."</td>";
		  echo "<td>".$row['time_out']."</td>";
		  echo "<td>".$row['status']."</td>";
		  echo "<td>".$row['teacher']."</td>";
		  echo "</tr>";
		  }
		  
		  mysqli_close($mysqli);
		?>

	</tbody>
	</table>
</div>

<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>