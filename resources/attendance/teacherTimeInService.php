<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>
<?php

require_once(realpath(dirname(__FILE__) . "/../config.php"));
$date = date('d-M-Y');

if (isset($_POST['idNumber'])) {

	$result = mysqli_query($mysqli,"SELECT * FROM teacher_attendance WHERE account_id='".$_SESSION['accountId']."' AND date='".$date."'");
	$count = mysqli_num_rows($result);
	
	if ($count == 1) {
		
		mysqli_close($mysqli);

		$response = array(
			"message" => "You have already timed in for today!"
		);
		echo json_encode($response);
		
	} else {
		
		$sql="INSERT INTO teacher_attendance (account_id, id_number, date, time_in)
		VALUES
		('".$_SESSION['accountId']."', '".$_POST['idNumber']."', '".$_POST['date']."', '".$_POST['timeIn']."')";

		if (!mysqli_query($mysqli,$sql)) {
			die('Error: ' . mysqli_error($mysqli));
		}
		
		mysqli_close($mysqli);
		
		$response = array(
			"message" => "Successfully timed in!"
		);
		echo json_encode($response);
		
	}

}

?>