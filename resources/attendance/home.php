<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";
    $typedjs = "1";

	$title = "Readability Center";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");

    $result = mysqli_query($mysqli,"SELECT * FROM banner");
    $row = mysqli_fetch_array($result);
?>

<style>
/* code for animated blinking cursor */
.typed-cursor{
    opacity: 1;
    font-weight: 100;
    -webkit-animation: blink 0.7s infinite;
    -moz-animation: blink 0.7s infinite;
    -ms-animation: blink 0.7s infinite;
    -o-animation: blink 0.7s infinite;
    animation: blink 0.7s infinite;
}
@-keyframes blink{
    0% { opacity:1; }
    50% { opacity:0; }
    100% { opacity:1; }
}
@-webkit-keyframes blink{
    0% { opacity:1; }
    50% { opacity:0; }
    100% { opacity:1; }
}
@-moz-keyframes blink{
    0% { opacity:1; }
    50% { opacity:0; }
    100% { opacity:1; }
}
@-ms-keyframes blink{
    0% { opacity:1; }
    50% { opacity:0; }
    100% { opacity:1; }
}
@-o-keyframes blink{
    0% { opacity:1; }
    50% { opacity:0; }
    100% { opacity:1; }
}

body{
    background-color: #333;
}

.wrap{
    max-width: 800px;
    margin:150px auto;
}

.type-wrap{
    margin:10px auto;
    padding:20px;
    background:#f0f0f0;
    border-radius:5px;
    border:#CCC 1px solid;
}

.type-wrap #typed{
    font-size: 36px;
}
.type-wrap .typed-cursor{
    font-size: 36px;
}

</style>

<script type="text/javascript">

  $(function(){

    $("#typed").typed({
        strings: ["Welcome to <strong>READABILITY CENTER - SF</strong>.", "improving reading. improving learning.", "www.readabilitycenter.com", "info@readabilitycenter.com"],
        typeSpeed: 30,
        backDelay: 500,
        loop: true  ,
        contentType: 'html', // or text
        // defaults to false for infinite loop
        loopCount: false,
        callback: function(){ foo(); },
        resetCallback: function() { newTyped(); }
    });

    $(".reset").click(function(){
        $("#typed").typed('reset');
    });

  });

  function newTyped(){ /* A new typed object */ }

  function foo(){ console.log("Callback"); }

</script>

<div class="wrap">
  <div class="type-wrap">
    <span id="typed" style="white-space:pre;"></span>
  </div>
</div>

<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>