<?php 
session_start();
session_destroy();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<meta charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Login</title>
    <?php
	// CSS
	$bootstrapcss = "1";
	$signincss = "1";
	?>
    <?php 
		require_once(realpath(dirname(__FILE__) . "/../config.php")); 
		require_once(PUBLIC_HTML_PATH . "/css/readability.css");
		require_once(LIBRARY_PATH . "/extcss.php"); 
	?>
    <script src="../../public_html/js/jquery-2.1.4.min.js"></script>
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Sign in to continue to Readability</h1>
            <div class="account-wall">
                <img class="profile-img" src="../../public_html/images/readability_logo.png" alt="">
                <form action="checklogin.php" method="post" name="login_form" class="form-signin">
                <input type="text" name="email" class="form-control" placeholder="Name" required autofocus>
                <input type="password" name="password" class="form-control" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                <!--
                <label class="checkbox pull-left">
                    <input type="checkbox" value="remember-me">Remember me
                </label>
                <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
                -->
                </form>
            </div>
            <!--
            <a href="#" class="text-center new-account">Create an account </a>
            -->
        </div>
    </div>
</div>

<?php
	require_once(LIBRARY_PATH . "/extjs.php");
	require_once(PUBLIC_HTML_PATH . "/js/readability.js");
?>
</body>
</html>