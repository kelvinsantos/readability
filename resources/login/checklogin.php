<?php
session_start();
require_once(realpath(dirname(__FILE__) . "/../config.php")); 

// username and password sent from form 
$email=$_POST['email']; 
$password=$_POST['password']; 

// To protect MySQL injection (more detail about MySQL injection)
$email = stripslashes($email);
$password = stripslashes($password);
$email = mysql_real_escape_string($email);
$password = mysql_real_escape_string($password);

$result = mysqli_query($mysqli,"SELECT * FROM accounts WHERE email='$email' and password='$password'");
$row = mysqli_fetch_array($result);
$role = $row["role"];

if ($role == "student") {

	$resultb = mysqli_query($mysqli,"SELECT * FROM accounts AS a INNER JOIN student_information AS b ON a.account_id = b.account_id WHERE a.email='$email' and a.password='$password'");
	$countb = mysqli_num_rows($resultb);

	if ($countb == 1) {

		while($rowb = mysqli_fetch_array($resultb))
		{
			$isDeleted = $rowb["is_deleted"] == "1" ? true : false;
			if (!$isDeleted) {
				$_SESSION["accountId"] = $rowb["account_id"];
				$_SESSION["fullName"] = $rowb["full_name"];
				$_SESSION["role"] = $rowb["role"];
				$_SESSION["isAdmin"] = $rowb["admin_role"];
				$_SESSION["isLogged"] = true;
				
				header("location: ../attendance/home.php");

			} else {
				echo "<script>
				alert('This account has been deleted!');
				window.location.reload();
				</script>";
			}
		}

	} else {
		
		echo "<script>
		alert('Invalid login details.');
		window.location.href = 'index.php';
		</script>";

	}

} else if ($role == "teacher") {

	$resultb = mysqli_query($mysqli,"SELECT * FROM accounts AS a INNER JOIN teacher_information AS b ON a.account_id = b.account_id WHERE a.email='$email' and a.password='$password'");
	$countb = mysqli_num_rows($resultb);

	if ($countb == 1) {

		while($rowb = mysqli_fetch_array($resultb))
		{
			$isDeleted = $rowb["is_deleted"] == "1" ? true : false;
			if (!$isDeleted) {
				$_SESSION["accountId"] = $rowb["account_id"];
				$_SESSION["fullName"] = $rowb["full_name"];
				$_SESSION["role"] = $rowb["role"];
				$_SESSION["isAdmin"] = $rowb["admin_role"];
				$_SESSION["isLogged"] = true;
				
				header("location: ../attendance/home.php");

			} else {
				echo "<script>
				alert('This account has been deleted!');
				window.location.reload();
				</script>";
			}
		}

	} else {
		
		echo "<script>
		alert('Invalid login details.');
		window.location.href = 'index.php';
		</script>";

	}

} else if ($role == "admin") {

	$resultb = mysqli_query($mysqli,"SELECT * FROM accounts WHERE email='$email' and password='$password'");
	$countb = mysqli_num_rows($resultb);

	if ($countb == 1) {

		while($rowb = mysqli_fetch_array($resultb))
		{
			$isDeleted = $rowb["is_deleted"] == "1" ? true : false;
			if (!$isDeleted) {
				$_SESSION["accountId"] = $rowb["account_id"];
				$_SESSION["fullName"] = "Administrator";
				$_SESSION["role"] = $rowb["role"];
				$_SESSION["isAdmin"] = $rowb["admin_role"];
				$_SESSION["isLogged"] = true;
				
				header("location: ../attendance/home.php");

			} else {
				echo "<script>
				alert('This account has been deleted!');
				window.location.reload();
				</script>";
			}
		}

	} else {
		
		echo "<script>
		alert('Invalid login details.');
		window.location.href = 'index.php';
		</script>";

	}

} else {

	echo "<script>
	alert('Invalid login details.');
	window.location.href = 'index.php';
	</script>";

}
?>
