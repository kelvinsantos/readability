<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";

	$title = "Teacher Attendance Archive";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");
	
    $result = mysqli_query($mysqli,"SELECT * FROM teacher_information a INNER JOIN accounts b ON a.account_id = b.account_id WHERE a.account_id='".$_GET['accountId']."'");
	$row = mysqli_fetch_array($result);
?>

<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var printWindow = window.open();
        printWindow.document.write('<html>');
		printWindow.document.write('	<head>');
		printWindow.document.write('		<title>Teacher Information Sheet</title>');
		printWindow.document.write('		<link rel="stylesheet" href="../../public_html/css/bootstrap.min.css">');
		printWindow.document.write('	</head>');
        printWindow.document.write('<body>');
		printWindow.document.write('<br>');
        printWindow.document.write(data);
		printWindow.document.write(jQuery(".printFooter").html());
        printWindow.document.write('</body>');
		printWindow.document.write('</html>');
        printWindow.print();
        printWindow.close();
		window.location.reload();

        return true;
    }

</script>

<div class="container">
<form class="form-horizontal" role="form">
<h3><span class="label label-primary">Teacher Information</span></h3>
<br />
<button type="button" class="btn btn-primary" onclick="PrintElem('.table-responsive')">
  <span class="glyphicon glyphicon-print"></span> Print
</button>
<a href="../information/teacherInformationList.php">
<button type="button" class="btn btn-primary pull-right">
  <span class="glyphicon glyphicon-repeat"></span> Back to Teacher Attendance Archive
</button>
</a>
<br />
<br />
<div class="table-responsive">
    <table class="table" width="100%" cellpadding="0" cellspacing="0" border="1">
       <tr>
        <td class="col-md-2">
            <label class="form-control-static">Account Name</label>
        </td>
        <td class="col-md-2">
            <p class="form-control-static"><?php echo $row['email'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Account Password</label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['full_name'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Id Number</label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['password'] ?></p>
        </td>
      </tr>

      <tr>
        <td class="col-md-3">
            <label class="form-control-static">Full Name of Teacher</label>
        </td>
        <td class="col-md-3">
            <p class="form-control-static"><?php echo $row['id_number'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Nickname</label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['nick_name'] ?></p>
        </td>
      </tr>
      
      <tr>
      	<td>
        	<label class="form-control-static">Date of Birth</label>
        </td>
        <td>
        	<p class="form-control-static"><?php echo $row['date_of_birth'] ?></p>
        </td>
      	<td>
        	<label class="form-control-static">Age</label>
        </td>
        <td>
        	<p class="form-control-static"><?php echo $row['age_years'] ?> years</p>
        </td>
        <td colspan="2">
        	<p class="form-control-static"><?php echo $row['age_months'] ?> months</p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Home Address</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['home_address'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Phone</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['phone'] ?></p>
        </td>
      </tr>

    </table>
    
</div>

<div class="printFooter" style="display:none">
    
    <?php echo $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>

</div>

</form>
</div>
<?php mysqli_close($mysqli); ?>
<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>