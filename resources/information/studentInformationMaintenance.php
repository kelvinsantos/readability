<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	$bootstrapdatepickercss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";
	$bootstrapdatepicker = "1";
	$bootbox = "1";

	$title = "Student List";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");
	
	$accountId = "";
	if (isset($_GET['accountId'])) {
		$accountId = $_GET['accountId'];
	}
	
	if ($accountId != null && $accountId != "") {
		$result = mysqli_query($mysqli, "SELECT * FROM student_information a INNER JOIN accounts b ON a.account_id = b.account_id WHERE a.account_id='".$accountId."'");
		$row = mysqli_fetch_array($result);
	}
?>

<script type="text/javascript">
$(function() {
	$('#dateOfBirth').datepicker({
		format: 'yyyy-mm-dd'
	});
	
	$('.datepicker.dropdown-menu').click(function() {
		$(this).hide();
	});

	$("input[name='areThereMembersOfTheFamilyWhoHaveLearningDifficulties']").click(function() {
		if ($(this).val() == 0) {
			$("#ifYesPleaseSpecifyTheDifficultyAndTheRelationShipToThe").removeAttr("required").attr("readonly", "readonly").val("");
		} else {
			$("#ifYesPleaseSpecifyTheDifficultyAndTheRelationShipToThe").removeAttr("readonly").attr("required", "required");
		}
	});

	$("input[name='doesTheStudentHaveRemedialTeachingAtSchoolOrElseWher']").click(function() {
		if ($(this).val() == 0) {
			$("#ifYesStateWhereAndHowOften").removeAttr("required").attr("readonly", "readonly").val("");
		} else {
			$("#ifYesStateWhereAndHowOften").removeAttr("readonly").attr("required", "required");
		}
	});

	$("input[name='hasTheStudentBeenEvaluatedByADevelopmentalPediatricianO']").click(function() {
		if ($(this).val() == 0) {
			$("#when").removeAttr("required").attr("readonly", "readonly");
		} else {
			$("#when").removeAttr("readonly").attr("required", "required");
		}
	});

	$("input[name='areThereMembersOfTheFamilyWhoHaveLearningDifficulties']").click(function() {
		if ($(this).val() == 0) {
			$("#ifYesPleaseSpecifyTheDifficultyAndTheRelationShipToThe").removeAttr("required").attr("readonly", "readonly").val("");
		} else {
			$("#ifYesPleaseSpecifyTheDifficultyAndTheRelationShipToThe").removeAttr("readonly").attr("required", "required");
		}
	});

	$("input[name='visionTested']").click(function() {
		if ($(this).val() == 0) {
			$("#dateAndResults").removeAttr("required").attr("readonly", "readonly").val("");
		} else {
			$("#dateAndResults").removeAttr("readonly").attr("required", "required");
		}
	});

	$("input[name='hearingTested']").click(function() {
		if ($(this).val() == 0) {
			$("#hearingTestedDateAndResults").removeAttr("required").attr("readonly", "readonly").val("");
		} else {
			$("#hearingTestedDateAndResults").removeAttr("readonly").attr("required", "required");
		}
	});

	$("#save").click(function() {
		$.ajax({
		  method: "POST",
		  url: "studentInformationMaintenanceService.php",
		  data: jQuery("form").serialize(),
		  success: function(response) {
		  		var response = $.parseJSON(response);
		  		if (response.action == "insert") {
		  			bootbox.alert(response.message, function() {
						window.location.href='studentInformationList.php';
					});
		  		} else {
		  			bootbox.alert(response.message, function() {
						window.location.href='studentInformationMaintenance.php?accountId=' + response.accountId;
					});
		  		}
			}
		});
	});
});
</script>

<div class="container">
<form class="form-horizontal" method="post" role="form">
<?php if(isset($row['account_id'])) { ?>
<input type="hidden" id="accountId" name="accountId" value="<?php echo $row['account_id'] ?>" />
<?php } else { ?>
<input type="hidden" id="accountId" name="accountId" value="0" />
<?php } ?>
<h3>
    <span class="label label-primary">Student Information Maintenance</span>
</h3>
<a href="../information/studentInformationList.php">
<button type="button" class="btn btn-primary pull-right">
  <span class="glyphicon glyphicon-repeat"></span> Back to Student Attendance Archive
</button>
</a>
<br />
<br />
<br />
<div class="table-responsive">
    <table class="table" width="100%" cellpadding="0" cellspacing="0">

      <tr>
        <td class="col-md-2">
            <label class="form-control-static">Account Name</label>
        </td>
        <td class="col-md-2">
            <p class="form-control-static">
                <input type="text" class="form-control" id="accountEmail" name="accountEmail" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['email']; } ?>" required autofocus />
            </p>
        </td>
        <td>
            <label class="form-control-static">Account Password</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="accountPassword" name="accountPassword" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['password']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Id Number</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="idNumber" name="idNumber" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['id_number']; } ?>" required />
            </p>
        </td>
      </tr>
    
      <tr>
        <td class="col-md-3">
            <label class="form-control-static">Full Name of Student</label>
        </td>
        <td class="col-md-3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="fullName" name="fullName" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['full_name']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Nickname</label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="nickName" name="nickName" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['nick_name']; } ?>" required />
            </p>
        </td>
      </tr>
      
      <tr>
      	<td>
        	<label class="form-control-static">Date of Birth</label>
        </td>
        <td>
        	<p class="form-control-static">
                <input type="text" class="form-control" id="dateOfBirth" name="dateOfBirth" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['date_of_birth']; } ?>" required />
            </p>
        </td>
      	<td>
        	<label class="form-control-static">Age</label>
        </td>
        <td>
        	<p class="form-control-static">
                <input type="text" class="form-control" id="years" name="years" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['age_years']; } ?>" required /> years
            </p>
        </td>
        <td colspan="2">
        	<p class="form-control-static">
                <input type="text" class="form-control" id="months" name="months" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['age_months']; } ?>" required /> months
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Home Address</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <input type="text" class="form-control" name="homeAddress" id="homeAddress" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['home_address']; } ?>" required />
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Phone</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <input type="text" class="form-control" id="phone" name="phone" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['phone']; } ?>" required />
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">School</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="school" name="school" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['school']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Level</label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="level" name="level" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['level']; } ?>" required />
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">School Address</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <input type="text" class="form-control" id="schoolAddress" name="schoolAddress" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['school_address']; } ?>" required />
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Contact person in school and designation</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <input type="text" class="form-control" id="contactPersonInSchoolAndDesignation" 
                name="contactPersonInSchoolAndDesignation" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['contact_person_in_school_and_designation']; } ?>" required />
            </p>
        </td>
      </tr>

      <tr>
        <td>
            <label class="form-control-static">Father's Name</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <input type="text" class="form-control" id="fatherName" name="fatherName" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['father_name']; } ?>" required />
            </p>
        </td>
      </tr>

      <tr>
        <td>
            <label class="form-control-static">Contact Numbers: Mobile</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="fatherMobile" name="fatherMobile" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['father_mobile']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Office</label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="fatherOffice" name="fatherOffice" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['father_office']; } ?>" required />
            </p>
        </td>
      </tr>

      <tr>
        <td>
            <label class="form-control-static">Email Address</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="fatherEmail" 
                name="fatherEmail" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['father_email']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Occupation</label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="fatherOccupation" name="fatherOccupation" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['father_occupation']; } ?>" required />
            </p>
        </td>
      </tr>

      <tr>
        <td>
            <label class="form-control-static">Mother's Name</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <input type="text" class="form-control" id="motherName" name="motherName" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['mother_name']; } ?>" required />
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Contact Numbers: Mobile</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="motherMobile" name="motherMobile" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['mother_mobile']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Office</label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="motherOffice" name="motherOffice" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['mother_office']; } ?>" required />
            </p>
        </td>
      </tr>

      <tr>
        <td>
            <label class="form-control-static">Email Address</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="motherEmail" 
                name="motherEmail" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['mother_email']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Occupation</label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="motherOccupation" name="motherOccupation" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['mother_occupation']; } ?>" required />
            </p>
        </td>
      </tr>

      <tr>
        <td>
            <label class="form-control-static">Number of Siblings</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="numberOfSiblings" 
                name="numberOfSiblings" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['number_of_siblings']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Child's Position in the Family</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="childsPositionInTheFamily" name="childsPositionInTheFamily" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['childs_position_in_the_family']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Language/s Spoken at Home</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="languagesSpokenAtHome" name="languagesSpokenAtHome" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['languages_spoken_at_home']; } ?>" required />
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Observed Difficulties at Home</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <textarea class="form-control" id="observedDifficultiesAtHome" name="observedDifficultiesAtHome" required /><?php if ($accountId != null && $accountId != "") { echo $row['observed_difficulties_at_home']; } ?></textarea>
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Reported Difficulties at School</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <textarea class="form-control" id="reportedDifficultiesAtSchool" name="reportedDifficultiesAtSchool" required /><?php if ($accountId != null && $accountId != "") { echo $row['reported_difficulties_at_school']; } ?></textarea>
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Are there members of the family who have learning difficulties?</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
            	<?php
				$areThereMembersOfTheFamilyWhoHaveLearningDifficulties = "0";
				if ($accountId != null && $accountId != "") {
					$areThereMembersOfTheFamilyWhoHaveLearningDifficulties = $row['are_there_members_of_the_family_who_have_learning_difficulties'];
				}
				?>
                <input type="radio" name="areThereMembersOfTheFamilyWhoHaveLearningDifficulties" value="1" 
				<?php if ($areThereMembersOfTheFamilyWhoHaveLearningDifficulties == "1") { echo 'checked'; } ?>>Yes<br>
                <input type="radio" name="areThereMembersOfTheFamilyWhoHaveLearningDifficulties" value="0"
                <?php if ($areThereMembersOfTheFamilyWhoHaveLearningDifficulties == "0") { echo 'checked'; } ?>>No
            </p>
        </td>
      </tr>
      
     <tr>
        <td>
            <label class="form-control-static">If yes, please specify the difficulty and the relationship to the student</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
            	<textarea class="form-control" name="ifYesPleaseSpecifyTheDifficultyAndTheRelationShipToThe" 
                id="ifYesPleaseSpecifyTheDifficultyAndTheRelationShipToThe" readonly="readonly" /><?php if ($accountId != null && $accountId != "") { 
				echo $row['if_yes_please_specify_the_difficulty_and_the_relationship_to_the']; } ?></textarea>
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Does the student have remedial teaching at school or elsewhere?</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
            	<?php
				$doesTheStudentHaveRemedialTeachingAtSchoolOrElseWher = "0";
				if ($accountId != null && $accountId != "") {
					$doesTheStudentHaveRemedialTeachingAtSchoolOrElseWher = $row['does_the_student_have_remedial_teaching_at_school_or_elsewhere'];
				}
				?>
                <input type="radio" name="doesTheStudentHaveRemedialTeachingAtSchoolOrElseWher" value="1" 
				<?php if ($doesTheStudentHaveRemedialTeachingAtSchoolOrElseWher == "1") { echo 'checked'; } ?>>Yes<br>
                <input type="radio" name="doesTheStudentHaveRemedialTeachingAtSchoolOrElseWher" value="0"
                <?php if ($doesTheStudentHaveRemedialTeachingAtSchoolOrElseWher == "0") { echo 'checked'; } ?>>No
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">If yes, state where and how often</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
            <textarea class="form-control" name="ifYesStateWhereAndHowOften" id="ifYesStateWhereAndHowOften" readonly="readonly" /
            ><?php if ($accountId != null && $accountId != "") { echo $row['if_yes_state_where_and_how_often']; } ?></textarea>
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">
            	Has the student been evaluated by a developmental pediatrician or a neuropsychologist?
            <label>
        </td>
        <td>
            <p class="form-control-static">
            	<?php
				$hasTheStudentBeenEvaluatedByADevelopmentalPediatricianO = "0";
				if ($accountId != null && $accountId != "") {
					$hasTheStudentBeenEvaluatedByADevelopmentalPediatricianO = $row['has_the_student_been_evaluated_by_a_developmental_pediatrician_o'];
				}
				?>
                <input type="radio" name="hasTheStudentBeenEvaluatedByADevelopmentalPediatricianO" value="1" 
				<?php if ($hasTheStudentBeenEvaluatedByADevelopmentalPediatricianO == "1") { echo 'checked'; } ?>>Yes<br>
                <input type="radio" name="hasTheStudentBeenEvaluatedByADevelopmentalPediatricianO" value="0"
                <?php if ($hasTheStudentBeenEvaluatedByADevelopmentalPediatricianO == "0") { echo 'checked'; } ?>>No
            </p>
        </td>
        <td>
            <label class="form-control-static">When?<label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="when" name="when" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['when_question']; } ?>" readonly="readonly" />
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Name of evaluator<label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="nameOfEvaluator" name="nameOfEvaluator" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['name_of_evaluator']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Is the report available?<label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
            	<?php
				$isTheReportAvailable = "0";
				if ($accountId != null && $accountId != "") {
					$isTheReportAvailable = $row['is_the_report_available'];
				}
				?>
                <input type="radio" name="isTheReportAvailable" value="1" 
				<?php if ($isTheReportAvailable == "1") { echo 'checked'; } ?>>Yes<br>
                <input type="radio" name="isTheReportAvailable" value="0"
                <?php if ($isTheReportAvailable == "0") { echo 'checked'; } ?>>No
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Vision tested?<label>
        </td>
        <td>
            <p class="form-control-static">
            	<?php
				$visionTested = "0";
				if ($accountId != null && $accountId != "") {
					$visionTested = $row['vision_tested'];
				}
				?>
                <input type="radio" name="visionTested" value="1" 
				<?php if ($isTheReportAvailable == "1") { echo 'checked'; } ?>>Yes<br>
                <input type="radio" name="visionTested" value="0"
                <?php if ($visionTested == "0") { echo 'checked'; } ?>>No
            </p>
        </td>
        <td>
            <label class="form-control-static">Date and results<label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <textarea class="form-control" name="dateAndResults" id="dateAndResults" readonly="readonly" /><?php if ($accountId != null && $accountId != "") { echo $row['date_and_results']; } ?></textarea>
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Hearing tested?<label>
        </td>
        <td>
            <p class="form-control-static">
            	<?php
				$hearingTested = "0";
				if ($accountId != null && $accountId != "") {
					$hearingTested = $row['hearing_tested'];
				}
				?>
                <input type="radio" name="hearingTested" value="1" 
				<?php if ($hearingTested == "1") { echo 'checked'; } ?>>Yes<br>
                <input type="radio" name="hearingTested" value="0"
                <?php if ($hearingTested == "0") { echo 'checked'; } ?>>No
            </p>
        </td>
        <td>
            <label class="form-control-static">Date and results<label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <textarea class="form-control" name="hearingTestedDateAndResults" id="hearingTestedDateAndResults" readonly="readonly" /><?php if ($accountId != null && $accountId != "") { echo $row['hearing_tested_date_and_results']; } ?></textarea>
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">What is the student's general attitude towards school and learning?</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <textarea class="form-control" name="whatIsTheStudentsGeneralAttitudeTowardsSchoolAndLearnin" 
                id="whatIsTheStudentsGeneralAttitudeTowardsSchoolAndLearnin" required /><?php if ($accountId != null && $accountId != "") { 
                echo $row['what_is_the_students_general_attitude_towards_school_and_learnin']; } ?></textarea>
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Your child's interests</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <textarea class="form-control" name="yourChildsInterests" id="yourChildsInterests" required /><?php if ($accountId != null && $accountId != "") { echo $row['your_childs_interests']; } ?></textarea>
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Concern/s you want us to know</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <textarea class="form-control" name="concernsYouWantUsToKnow" id="concernsYouWantUsToKnow" required /><?php if ($accountId != null && $accountId != "") { echo $row['concerns_you_want_us_to_know']; } ?></textarea>
            </p>
        </td>
      </tr>
      
      <tr>
      	<td colspan="6">
        	<button type="button" class="btn btn-primary pull-right" name="save" id="save">Submit</button>	
        </td>
      </tr>

    </table>
    
</div>

</form>
</div>
<?php 
if ($accountId != null && $accountId != "") { 
	"}";
}
 ?>
<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>