<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";

	$title = "Teacher List";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");
	
	if (isset($_GET['op'])) {
		if ($_GET['op'] == 'deleteTeacher') {
			$sql="UPDATE accounts SET is_deleted = '1' WHERE account_id = '".$_GET['accountId']."'";
		
			if (!mysqli_query($mysqli,$sql)) {
				die('Error: ' . mysqli_error($mysqli));
			}
			
			mysqli_close($mysqli);
					
			echo "<script>
			alert('Delete Success!');
			window.location.href = 'teacherInformationList.php';
			</script>";

			break;
		}
	}
?>

<?php
    require_once 'Paginator.class.php';
 
    $limit      = ( isset( $_GET['limit'] ) ) ? $_GET['limit'] : 10;
    $page       = ( isset( $_GET['page'] ) ) ? $_GET['page'] : 1;
    $links      = ( isset( $_GET['links'] ) ) ? $_GET['links'] : 4;
    $query      = "SELECT * FROM accounts AS a INNER JOIN teacher_information AS b ON a.account_id = b.account_id WHERE a.role = 'Teacher' AND a.is_deleted = '0' ORDER BY b.id_number ASC";
 
    $Paginator  = new Paginator( $mysqli, $query );
 
    $results    = $Paginator->getData( $limit, $page );
?>

<script type="text/javascript">
$(document).ready(function() {
	$("#addTeacher").click(function() {
		window.location.href = "teacherInformationMaintenance.php";	
	});
});

</script>

<div class="container">
<form class="form-horizontal" method="post" role="form">
<h3><span class="label label-primary"><?php echo $title ?></span></h3>
<br />
<div class="table-responsive">
<table class="table table-striped">
	<thead>
        <tr>
        <th width="15%">ID Number</th>
        <th width="15%">Name</th>
        <th width="15%">Date of Birth</th>
        <th width="15%">Age</th>
        <th width="10%">Action</th>
    </tr>
    </thead>
    <tbody>
    	<?php for( $i = 0; $i < count( $results->data ); $i++ ) : ?>
        <tr>
            <td><?php echo $results->data[$i]['id_number']; ?></td>
            <td><?php echo $results->data[$i]['full_name']; ?></td>
            <td><?php echo $results->data[$i]['date_of_birth']; ?></td>
            <td><?php echo $results->data[$i]['age_years'] . " years and " . $results->data[$i]['age_months']. " months"; ?></td>
            <td><a href="../information/teacherInformation.php?accountId=<?php echo $results->data[$i]['account_id']; ?>">View</a> | <a href="teacherInformationMaintenance.php?accountId=<?php echo $results->data[$i]['account_id']; ?>">Edit</a> | <a href="teacherInformationList.php?op=deleteStudent&accountId=<?php echo $results->data[$i]['account_id']; ?>">Delete</a></td>
        </tr>
		<?php endfor; ?>
		<tr>
			<td colspan="5"><div class="pull-right"><?php echo $Paginator->createLinks( $links, 'pagination pagination-sm' ); ?></div></td>
		</tr>
    </tbody>
</table>
<button type="button" class="btn btn-primary pull-right" name="addTeacher" id="addTeacher">Add Teacher</button>
</div>
</form>
</div>

<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>