<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	$bootstrapdatepickercss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";
	$bootstrapdatepicker = "1";
	$bootbox = "1";

	$title = "Teacher List";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");
	
	$accountId = "";
	if (isset($_GET['accountId'])) {
		$accountId = $_GET['accountId'];
	}
	
	if ($accountId != null && $accountId != "") {	
		$result = mysqli_query($mysqli, "SELECT * FROM teacher_information a INNER JOIN accounts b ON a.account_id = b.account_id WHERE a.account_id='".$accountId."'");
		$row = mysqli_fetch_array($result);
	}
?>

<script type="text/javascript">
$(document).ready(function() {
	$('#dateOfBirth').datepicker({
		format: 'yyyy-mm-dd'
	});
	
	$('.datepicker.dropdown-menu').click(function() {
		$(this).hide();
	});

	$("#save").click(function() {
		$.ajax({
		  method: "POST",
		  url: "teacherInformationMaintenanceService.php",
		  data: jQuery("form").serialize(),
		  success: function(response) {
		  		var response = $.parseJSON(response);
		  		if (response.action == "insert") {
		  			bootbox.alert(response.message, function() {
						window.location.href='teacherInformationList.php';
					});
		  		} else {
		  			bootbox.alert(response.message, function() {
						window.location.href='teacherInformationMaintenance.php?accountId=' + response.accountId;
					});
		  		}
			}
		});
	});
});
</script>

<div class="container">
<form class="form-horizontal" method="post" role="form">
<?php if(isset($row['account_id'])) { ?>
<input type="hidden" id="accountId" name="accountId" value="<?php echo $row['account_id'] ?>" />
<?php } else { ?>
<input type="hidden" id="accountId" name="accountId" value="0" />
<?php } ?>
<h3>
    <span class="label label-primary">Teacher Information Maintenance</span>
</h3>
<a href="../information/teacherInformationList.php">
<button type="button" class="btn btn-primary pull-right">
  <span class="glyphicon glyphicon-repeat"></span> Back to Teacher Attendance Archive
</button>
</a>
<br />
<br />
<br />
<div class="table-responsive">
    <table class="table" width="100%" cellpadding="0" cellspacing="0">
	  
      <tr>
        <td class="col-md-2">
            <label class="form-control-static">Account Name</label>
        </td>
        <td class="col-md-2">
            <p class="form-control-static">
                <input type="text" class="form-control" id="accountEmail" name="accountEmail" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['email']; } ?>" required autofocus />
            </p>
        </td>
        <td>
            <label class="form-control-static">Account Password</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="accountPassword" name="accountPassword" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['password']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Id Number</label>
        </td>
        <td>
            <p class="form-control-static">
                <input type="text" class="form-control" id="idNumber" name="idNumber" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['id_number']; } ?>" required />
            </p>
        </td>
      </tr>
    
      <tr>
        <td class="col-md-3">
            <label class="form-control-static">Full Name of Teacher</label>
        </td>
        <td class="col-md-3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="fullName" name="fullName" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['full_name']; } ?>" required />
            </p>
        </td>
        <td>
            <label class="form-control-static">Nickname</label>
        </td>
        <td colspan="3">
            <p class="form-control-static">
                <input type="text" class="form-control" id="nickName" name="nickName" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['nick_name']; } ?>" required />
            </p>
        </td>
      </tr>
      
      <tr>
      	<td>
        	<label class="form-control-static">Date of Birth</label>
        </td>
        <td>
        	<p class="form-control-static">
                <input type="text" class="form-control" id="dateOfBirth" name="dateOfBirth" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['date_of_birth']; } ?>" required />
            </p>
        </td>
      	<td>
        	<label class="form-control-static">Age</label>
        </td>
        <td>
        	<p class="form-control-static">
                <input type="text" class="form-control" id="years" name="years" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['age_years']; } ?>" required /> years
            </p>
        </td>
        <td colspan="2">
        	<p class="form-control-static">
                <input type="text" class="form-control" id="months" name="months" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['age_months']; } ?>" required /> months
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Home Address</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <input type="text" class="form-control" name="homeAddress" id="homeAddress" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['home_address']; } ?>" required />
            </p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Phone</label>
        </td>
        <td colspan="5">
            <p class="form-control-static">
                <input type="text" class="form-control" id="phone" name="phone" 
                value="<?php if ($accountId != null && $accountId != "") { echo $row['phone']; } ?>" required />
            </p>
        </td>
      </tr>

      <tr>
      	<td colspan="6">
        	<button type="button" class="btn btn-primary pull-right" name="save" id="save">Submit</button>	
        </td>
      </tr>

    </table>
    
</div>

</form>
</div>
<?php 
if ($accountId != null && $accountId != "") { 
	"}";
}
 ?>
<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>