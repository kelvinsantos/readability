<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>
<?php

require_once(realpath(dirname(__FILE__) . "/../config.php"));

if (isset($_POST['accountId'])) {
	$accountId = $_POST['accountId'];
	if ($accountId != null && $accountId != "0") {
		
		$sql="UPDATE accounts SET email = '".$_POST['accountEmail']."', password = '".$_POST['accountPassword']."' 
			WHERE account_id = '".$_POST['accountId']."'";
		
		if (!mysqli_query($mysqli,$sql)) {
			die('Error: ' . mysqli_error($mysqli));
		}
		
		$sql="UPDATE student_information SET 
			id_number = '".$_POST['idNumber']."',
			full_name = '".$_POST['fullName']."',
			nick_name = '".$_POST['nickName']."',
			date_of_birth = '".$_POST['dateOfBirth']."',
			age_years = '".$_POST['years']."',
			age_months = '".$_POST['months']."',
			home_address = '".$_POST['homeAddress']."',
			phone = '".$_POST['phone']."',
			school = '".$_POST['school']."',
			level = '".$_POST['level']."',
			school_address = '".$_POST['schoolAddress']."',
			contact_person_in_school_and_designation = '".$_POST['contactPersonInSchoolAndDesignation']."',
			father_name = '".$_POST['fatherName']."',
			father_mobile = '".$_POST['fatherMobile']."',
			father_office = '".$_POST['fatherOffice']."',
			father_email = '".$_POST['fatherEmail']."',
			father_occupation = '".$_POST['fatherOccupation']."',
			mother_name = '".$_POST['motherName']."',
			mother_mobile = '".$_POST['motherMobile']."',
			mother_office = '".$_POST['motherOffice']."',
			mother_email = '".$_POST['motherEmail']."',
			mother_occupation = '".$_POST['motherOccupation']."',
			number_of_siblings = '".$_POST['numberOfSiblings']."',
			
			childs_position_in_the_family = '".$_POST['childsPositionInTheFamily']."',
			languages_spoken_at_home = '".$_POST['languagesSpokenAtHome']."',
			
			observed_difficulties_at_home = '".$_POST['observedDifficultiesAtHome']."',
			reported_difficulties_at_school = '".$_POST['reportedDifficultiesAtSchool']."',
			
			are_there_members_of_the_family_who_have_learning_difficulties = 
				'".$_POST['areThereMembersOfTheFamilyWhoHaveLearningDifficulties']."',
				
			if_yes_please_specify_the_difficulty_and_the_relationship_to_the = 
				'".$_POST['ifYesPleaseSpecifyTheDifficultyAndTheRelationShipToThe']."',
				
			does_the_student_have_remedial_teaching_at_school_or_elsewhere = 
				'".$_POST['doesTheStudentHaveRemedialTeachingAtSchoolOrElseWher']."',
				
			if_yes_state_where_and_how_often = '".$_POST['ifYesStateWhereAndHowOften']."',
			
			has_the_student_been_evaluated_by_a_developmental_pediatrician_o = 
				'".$_POST['hasTheStudentBeenEvaluatedByADevelopmentalPediatricianO']."',
				
			when_question = '".$_POST['when']."',
			name_of_evaluator = '".$_POST['nameOfEvaluator']."',
			is_the_report_available = '".$_POST['isTheReportAvailable']."',
			vision_tested = '".$_POST['visionTested']."',
			date_and_results = '".$_POST['dateAndResults']."',
			hearing_tested = '".$_POST['hearingTested']."',
			hearing_tested_date_and_results = '".$_POST['hearingTestedDateAndResults']."',
			
			what_is_the_students_general_attitude_towards_school_and_learnin = 
				'".$_POST['whatIsTheStudentsGeneralAttitudeTowardsSchoolAndLearnin']."',
				
			your_childs_interests = '".$_POST['yourChildsInterests']."',
			concerns_you_want_us_to_know = '".$_POST['concernsYouWantUsToKnow']."'
			WHERE account_id = $accountId";
			
			if (!mysqli_query($mysqli,$sql)) {
				die('Error: ' . mysqli_error($mysqli));
			}
				
			mysqli_close($mysqli);

			$response = array(
				"message" => "Update success!",
				"action" => "update",
				"accountId" => $accountId
			);
			echo json_encode($response);
			
	} else {
		
		$sql="INSERT INTO accounts (email, password, role) VALUES ('".$_POST['accountEmail']."', '".$_POST['accountPassword']."', 'student')";
		
		if (!mysqli_query($mysqli,$sql)) {
			die('Error: ' . mysqli_error($mysqli));
		}
		
		$accountId = $mysqli->insert_id;
		
		$sql="INSERT INTO student_information (
			account_id, 
			id_number, 
			full_name, 
			nick_name, 
			date_of_birth,
			age_years,
			age_months, 
			home_address, 
			phone, 
			school, 
			level, 
			school_address, 
			contact_person_in_school_and_designation, 
			father_name, father_mobile, 
			father_office, 
			father_email, 
			father_occupation,
			mother_name, 
			mother_mobile, 
			mother_office, 
			mother_email, 
			mother_occupation, 
			number_of_siblings, 
			childs_position_in_the_family, 
			languages_spoken_at_home,
			observed_difficulties_at_home, 
			reported_difficulties_at_school, 
			are_there_members_of_the_family_who_have_learning_difficulties, 
			if_yes_please_specify_the_difficulty_and_the_relationship_to_the, 
			does_the_student_have_remedial_teaching_at_school_or_elsewhere, 
			if_yes_state_where_and_how_often, 
			has_the_student_been_evaluated_by_a_developmental_pediatrician_o, 
			when_question, 
			name_of_evaluator, 
			is_the_report_available, 
			vision_tested, 
			date_and_results, 
			hearing_tested, 
			hearing_tested_date_and_results, 
			what_is_the_students_general_attitude_towards_school_and_learnin, 
			your_childs_interests, concerns_you_want_us_to_know)
		VALUES
			(
			'".$accountId."', 
			'".$_POST['idNumber']."', 
			'".$_POST['fullName']."', 
			'".$_POST['nickName']."', 
			'".$_POST['dateOfBirth']."', 
			'".$_POST['years']."', 
			'".$_POST['months']."', 
			'".$_POST['homeAddress']."', 
			'".$_POST['phone']."', 
			'".$_POST['school']."', 
			'".$_POST['level']."', 
			'".$_POST['schoolAddress']."', 
			'".$_POST['contactPersonInSchoolAndDesignation']."', 
			'".$_POST['fatherName']."', 
			'".$_POST['fatherMobile']."', 
			'".$_POST['fatherOffice']."', 
			'".$_POST['fatherEmail']."', 
			'".$_POST['fatherOccupation']."', 
			'".$_POST['motherName']."', 
			'".$_POST['motherMobile']."', 
			'".$_POST['motherOffice']."', 
			'".$_POST['motherEmail']."', 
			'".$_POST['motherOccupation']."', 
			'".$_POST['numberOfSiblings']."', 
			'".$_POST['childsPositionInTheFamily']."', 
			'".$_POST['languagesSpokenAtHome']."', 
			'".$_POST['observedDifficultiesAtHome']."', 
			'".$_POST['reportedDifficultiesAtSchool']."', 
			'".$_POST['areThereMembersOfTheFamilyWhoHaveLearningDifficulties']."', 
			'".$_POST['ifYesPleaseSpecifyTheDifficultyAndTheRelationShipToThe']."', 
			'".$_POST['doesTheStudentHaveRemedialTeachingAtSchoolOrElseWher']."', 
			'".$_POST['ifYesStateWhereAndHowOften']."', 
			'".$_POST['hasTheStudentBeenEvaluatedByADevelopmentalPediatricianO']."', 
			'".$_POST['when']."', 
			'".$_POST['nameOfEvaluator']."', 
			'".$_POST['isTheReportAvailable']."', 
			'".$_POST['visionTested']."', 
			'".$_POST['dateAndResults']."', 
			'".$_POST['hearingTested']."', 
			'".$_POST['hearingTestedDateAndResults']."', 
			'".$_POST['whatIsTheStudentsGeneralAttitudeTowardsSchoolAndLearnin']."', 
			'".$_POST['yourChildsInterests']."', 
			'".$_POST['concernsYouWantUsToKnow']."')";
			
			if (!mysqli_query($mysqli,$sql)) {
				die('Error: ' . mysqli_error($mysqli));
			}
				
			mysqli_close($mysqli);

			$response = array(
				"message" => "Insert success!",
				"action" => "insert"
			);
			echo json_encode($response);
			
	}
}

?>