<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";

	$title = "Student Attendance Archive";
	$useUpdateClock = false;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");
	
    $result = mysqli_query($mysqli,"SELECT * FROM student_information a INNER JOIN accounts b ON a.account_id = b.account_id WHERE a.account_id='".$_GET['accountId']."'");
	$row = mysqli_fetch_array($result);
?>

<script type="text/javascript">

    function PrintElem(elem)
    {
        Popup($(elem).html());
    }

    function Popup(data) 
    {
        var printWindow = window.open();
        printWindow.document.write('<html>');
		printWindow.document.write('	<head>');
		printWindow.document.write('		<title>Student Information Sheet</title>');
		printWindow.document.write('		<link rel="stylesheet" href="../../public_html/css/bootstrap.min.css">');
		printWindow.document.write('	</head>');
        printWindow.document.write('<body>');
		printWindow.document.write('<br>');
        printWindow.document.write(data);
		printWindow.document.write(jQuery(".printFooter").html());
        printWindow.document.write('</body>');
		printWindow.document.write('</html>');
        printWindow.print();
        printWindow.close();
		window.location.reload();

        return true;
    }

</script>

<div class="container">
<form class="form-horizontal" role="form">
<h3><span class="label label-primary">Student Information</span></h3>
<br />
<button type="button" class="btn btn-primary" onclick="PrintElem('.table-responsive')">
  <span class="glyphicon glyphicon-print"></span> Print
</button>
<a href="../information/studentInformationList.php">
<button type="button" class="btn btn-primary pull-right">
  <span class="glyphicon glyphicon-repeat"></span> Back to Student Attendance Archive
</button>
</a>
<br />
<br />
<div class="table-responsive">
    <table class="table" width="100%" cellpadding="0" cellspacing="0" border="1">
       <tr>
        <td class="col-md-2">
            <label class="form-control-static">Account Name</label>
        </td>
        <td class="col-md-2">
            <p class="form-control-static"><?php echo $row['email'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Account Password</label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['password'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Id Number</label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['id_number'] ?></p>
        </td>
      </tr>

      <tr>
        <td class="col-md-3">
            <label class="form-control-static">Full Name of Student</label>
        </td>
        <td class="col-md-3">
            <p class="form-control-static"><?php echo $row['full_name'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Nickname</label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['nick_name'] ?></p>
        </td>
      </tr>
      
      <tr>
      	<td>
        	<label class="form-control-static">Date of Birth</label>
        </td>
        <td>
        	<p class="form-control-static"><?php echo $row['date_of_birth'] ?></p>
        </td>
      	<td>
        	<label class="form-control-static">Age</label>
        </td>
        <td>
        	<p class="form-control-static"><?php echo $row['age_years'] ?> years</p>
        </td>
        <td colspan="2">
        	<p class="form-control-static"><?php echo $row['age_months'] ?> months</p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Home Address</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['home_address'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Phone</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['phone'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">School</label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['school'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Level</label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['level'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">School Address</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['school_address'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Contact person in school and designation</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['contact_person_in_school_and_designation'] ?></p>
        </td>
      </tr>

      <tr>
        <td>
            <label class="form-control-static">Father's Name</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['father_name'] ?></p>
        </td>
      </tr>
      <tr>
        <td>
            <label class="form-control-static">Contact Numbers: Mobile</label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['father_mobile'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Office</label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['father_office'] ?></p>
        </td>
      </tr>
      <tr>
        <td>
            <label class="form-control-static">Email Address</label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['father_email'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Occupation</label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['father_occupation'] ?></p>
        </td>
      </tr>
      <tr>
        <td>
            <label class="form-control-static">Mother's Name</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['mother_name'] ?></p>
        </td>
      </tr>
      <tr>
        <td>
            <label class="form-control-static">Contact Numbers: Mobile</label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['mother_mobile'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Office</label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['mother_office'] ?></p>
        </td>
      </tr>
      <tr>
        <td>
            <label class="form-control-static">Email Address</label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['mother_email'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Occupation</label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['mother_occupation'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Number of Siblings</label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['number_of_siblings'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Child's Position in the Family</label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['childs_position_in_the_family'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Language/s Spoken at Home</label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['languages_spoken_at_home'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Observed Difficulties at Home</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['observed_difficulties_at_home'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Reported Difficulties at School</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['reported_difficulties_at_school'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Are there members of the family who have learning difficulties?</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['are_there_members_of_the_family_who_have_learning_difficulties'] == "1" ? "Yes" : "No" ?></p>
        </td>
      </tr>
      
     <tr>
        <td>
            <label class="form-control-static">If yes, please specify the difficulty and the relationship to the student</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['if_yes_please_specify_the_difficulty_and_the_relationship_to_the'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Does the student have remedial teaching at school or elsewhere?</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['does_the_student_have_remedial_teaching_at_school_or_elsewhere'] == "1" ? "Yes" : "No" ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">If yes, state where and how often</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['if_yes_state_where_and_how_often'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">
            	Has the student been evaluated by a developmental pediatrician or a neuropsychologist?
            <label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['has_the_student_been_evaluated_by_a_developmental_pediatrician_o'] == "1" ? "Yes" : "No" ?></p>
        </td>
        <td>
            <label class="form-control-static">When?<label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['when_question'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Name of evaluator<label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['name_of_evaluator'] ?></p>
        </td>
        <td>
            <label class="form-control-static">Is the report available?<label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['is_the_report_available'] == "1" ? "Yes" : "No" ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Vision tested?<label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['vision_tested'] == "1" ? "Yes" : "No" ?></p>
        </td>
        <td>
            <label class="form-control-static">Date and results<label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['date_and_results'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Hearing tested?<label>
        </td>
        <td>
            <p class="form-control-static"><?php echo $row['hearing_tested'] == "1" ? "Yes" : "No" ?></p>
        </td>
        <td>
            <label class="form-control-static">Date and results<label>
        </td>
        <td colspan="3">
            <p class="form-control-static"><?php echo $row['hearing_tested_date_and_results'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">What is the student's general attitude towards school and learning?</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['what_is_the_students_general_attitude_towards_school_and_learnin'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Your child's interests</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['your_childs_interests'] ?></p>
        </td>
      </tr>
      
      <tr>
        <td>
            <label class="form-control-static">Concern/s you want us to know</label>
        </td>
        <td colspan="5">
            <p class="form-control-static"><?php echo $row['concerns_you_want_us_to_know'] ?></p>
        </td>
      </tr>

    </table>
    
</div>

<div class="printFooter" style="display:none">

    <table class="table" width="100%" cellpadding="0" cellspacing="0" align="center">
    	
        <tr>
        	<td>
            	<label class="text-center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;____________________</label>
            </td>
            <td>
            	<label class="text-center">____________________</label>
            </td>
        </tr>
        <tr>
        	<td>
            	<label class="text-center">Parent's Signature over Printed Name</label>
            </td>
            <td>
            	<label class="text-center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date</label>
            </td>
        </tr>
        
    </table>
    
    <?php echo $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>

</div>

</form>
</div>
<?php mysqli_close($mysqli); ?>
<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>