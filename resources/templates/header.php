<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
   "http://www.w3.org/TR/html4/strict.dtd">

<html lang="en">
<head>
	<meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="">
  <meta name="author" content="">
	<title><?php echo $title ?></title>
    
    <?php 
		require_once(PUBLIC_HTML_PATH . "/css/readability.css");
		require_once(LIBRARY_PATH . "/extcss.php");
		
		// Change the line below to your timezone!
		date_default_timezone_set('Asia/Manila');
		$date = date('d-M-Y');
    $current_date = date('H:i:s A');
		
		if (isset($jqueryjs)) {
			$jqueryjs = $jqueryjs;
		} else {
			$jqueryjs = "";
		}
		
		if ($jqueryjs == "1") {
			echo '<script src="../../public_html/js/jquery-2.1.4.min.js"></script>';
		}
		
		$disabledTimeOut=false;
    $role = $_SESSION['role'];
    if ($role == "student") {

      $result = mysqli_query($mysqli,"SELECT * FROM student_attendance WHERE account_id='".$_SESSION['accountId']."' AND date='".$date."' AND time_out IS NOT NULL");
      $count = mysqli_num_rows($result);
      if ($count==1) {
        $disabledTimeOut=true;
      }
      
      $result2 = mysqli_query($mysqli,"SELECT * FROM student_attendance WHERE account_id='".$_SESSION['accountId']."' AND date='".$date."'");
      $count2 = mysqli_num_rows($result2);
      if ($count2==0) {
        $disabledTimeOut=true;
      }

    } else if ($role == "teacher") {

      $result = mysqli_query($mysqli,"SELECT * FROM teacher_attendance WHERE account_id='".$_SESSION['accountId']."' AND date='".$date."' AND time_out IS NOT NULL");
      $count = mysqli_num_rows($result);
      if ($count==1) {
        $disabledTimeOut=true;
      }
      
      $result2 = mysqli_query($mysqli,"SELECT * FROM teacher_attendance WHERE account_id='".$_SESSION['accountId']."' AND date='".$date."'");
      $count2 = mysqli_num_rows($result2);
      if ($count2==0) {
        $disabledTimeOut=true;
      }

    }
		
	?>
    

    <?php if ($useUpdateClock) { ?>
    	<script type="text/javascript">
        //setInterval('updateClock()', 1000);
      </script>
    <?php } ?>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
</head>

<body>
    <div id="wrap">
      <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../attendance/home.php">Readability Center - SF</a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">

            <li <?php echo ($title == 'Readability Center' ? "class='active'" : ""); ?>><a href="../attendance/home.php">Home</a></li>

            <?php if ($role == "student") { ?>

              <li <?php echo ($title == 'Student Time In' ? "class='active'" : ""); ?>><a href="../attendance/studentTimeIn.php">Time In</a></li>
              <li <?php echo ($title == 'Student Time Out' ? "class='active'" : ""); echo ($disabledTimeOut == true ? "class='disabled'" : ""); ?>>
              <a <?php echo ($disabledTimeOut == true ? "href='#'" : "href='../attendance/studentTimeOut.php'"); ?>>Time Out</a></li>

            <?php } ?>

            <?php if ($role == "teacher") { ?>

              <li <?php echo ($title == 'Teacher Time In' ? "class='active'" : ""); ?>><a href="../attendance/teacherTimeIn.php">Time In</a></li>
              <li <?php echo ($title == 'Teacher Time Out' ? "class='active'" : ""); echo ($disabledTimeOut == true ? "class='disabled'" : ""); ?>>
              <a <?php echo ($disabledTimeOut == true ? "href='#'" : "href='../attendance/teacherTimeOut.php'"); ?>>Time Out</a></li>

            <?php } ?>

            <?php if ($role == "admin") { ?>

              <li <?php echo ($title == 'Student List' ? "class='active'" : ""); ?>><a href="../information/studentInformationList.php">Student List</a></li>
              <li <?php echo ($title == 'Student Attendance Archive' ? "class='active'" : ""); ?>><a href="../attendance/studentAttendanceArchive.php">Student Attendance</a></li>
              <li <?php echo ($title == 'Teacher List' ? "class='active'" : ""); ?>><a href="../information/teacherInformationList.php">Teacher List</a></li>
              <li <?php echo ($title == 'Teacher Attendance Archive' ? "class='active'" : ""); ?>><a href="../attendance/teacherAttendanceArchive.php">Teacher Attendance</a></li>
              <!--
                <li <?php echo ($title == 'Banner Maintenance' ? "class='active'" : ""); ?>><a href="../misc/bannerMaintenance.php">Banner Maintenance</a></li>
              -->

            <?php } ?>

            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['fullName']; ?> <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="../login/logout.php">Logout</a></li>
                </ul>
              </li>
            </ul>
          </div>
        </div>
      </div>