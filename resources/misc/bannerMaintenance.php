<?php require_once(realpath(dirname(__FILE__) . '/../templates/sessionHeader.php')) ?>

<?php
	// CSS
	$bootstrapcss = "1";
	$stickyfooternavbarcss = "1";
	
	// JS
	$jqueryjs = "1";
	$bootstrapjs = "1";
	$bootbox = "1";

	$title = "Banner Maintenance";
	$useUpdateClock = true;
	require_once(realpath(dirname(__FILE__) . "/../config.php"));
	require_once(TEMPLATES_PATH . "/header.php");

	$result = mysqli_query($mysqli,"SELECT * FROM banner");
	$row = mysqli_fetch_array($result);
?>

<script type="text/javascript">
$(function() {
	$("#save").click(function() {
		$.ajax({
		  method: "POST",
		  url: "bannerMaintenanceService.php",
		  data: jQuery("form").serialize(),
		  success: function(response) {
		  		var response = $.parseJSON(response);
				bootbox.alert(response.message, function() {
					window.location.href='bannerMaintenance.php';
				});
			}
		});
	});
});
</script>

<div class="container">
    <form method="POST" role="form" class="form-align">
        <h3><span class="label label-primary"><?php echo $title ?></span></h3>
        <br />
        <div class="form-group">
            <label>Banner Message</label>
            <input type="text" class="form-control" name="message" id="message" value="<?php echo $row['message']; ?>">
        </div>
        <button type="button" class="btn btn-primary pull-right" name="save" id="save">Submit</button>
    </form>
</div>
<?php mysqli_close($mysqli); ?>

<?php require_once(TEMPLATES_PATH . "/footer.php"); ?>