<?php

/* START OF ARRAY CONFIGURATIONS */

$config = array(
	"db" => array(
		"db1" => array(
			"dbname" => "readability",
			"username" => "root",
			"password" => "",
			"host" => "localhost"
		)
	),
	"urls" => array(
		"baseUrl" => "http://readabilitycenter.com/"
	),
	"paths" => array(
		"images" => array(
			"imagePath" => $_SERVER["DOCUMENT_ROOT"] . "/readability/public_html/images"
		)
	)
);

/* END OF ARRAY CONFIGURATIONS */

/* START OF CONFIGURATIONS */

// database settings
$connection = "db1";
$host = $config['db'][$connection]['host'];
$username = $config['db'][$connection]['username'];
$password = $config['db'][$connection]['password'];
$dbname = $config['db'][$connection]['dbname'];

// image path
$imagePath = $config['paths']['images']['imagePath'];

/* END OF CONFIGURATIONS */

/* START OF DATABASE CONNECTION */

/* Open a connection */
$mysqli = new mysqli($host, $username, $password, $dbname);

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

/* END OF DATABASE CONNECTION */

/* START OF VARIABLE DEFINITION */

defined("PUBLIC_HTML_PATH")
	or define("PUBLIC_HTML_PATH", realpath(dirname(__FILE__) . '/../public_html'));
	
defined("LIBRARY_PATH")
	or define("LIBRARY_PATH", realpath(dirname(__FILE__) . '/library'));
	
defined("TEMPLATES_PATH")
	or define("TEMPLATES_PATH", realpath(dirname(__FILE__) . '/templates'));
	
defined("ATTENDANCE_PATH")
	or define("ATTENDANCE_PATH", realpath(dirname(__FILE__) . '/attendance'));
	
/* END OF VARIABLE DEFINITION */

?>